// index.js - activity

/*
	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.
*/



/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
*/

//Solution: 

/*Debug*/

let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}


let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}


/*Debug and Update*/

/*
	 function introduce(student){
	 	console.log("Hi! I'm " + student.names + "." + "I am " + student.ages + " years old.")
	 	console.log("I study the following courses: " + student.class)
	 }
*/

// TRANSLATED VERSION ======>

const introduce = (student) => {
	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old.`);
	console.log(`I study the following courses: ${student.classes}`);
}

introduce(student1);
introduce(student2);

/*
	 function getCube(num){
	 	console.log(Math.pow(num,3));
	 }
*/

// TRANSLATED VERSION ======>

let getCube = (num) => console.log(Math.pow(num,3));

getCube(3);

// console.log("***");



let numArr = [15,16,32,21,21,2];

/*
	let numArr1 = numArr.forEach(function(num){
		console.log(num);
	})
	console.log(numArr1);
*/

// TRANSLATED VERSION ======>

let numArr2 = numArr.forEach(listNumArr = (num) => {
	console.log(num)}
	);
console.log(numArr2);

// console.log("***");

/*
	let numsSquared = numArr.map(function(num){
		return Math.pow(num,2);
	  }
	)
*/

// TRANSLATED VERSION ======>

let numsSquared = numArr.map(squared = (num) => Math.pow(num,2));

console.log(numsSquared);



/*2. Class Constructor

    Create a class constructor able to receive 4 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.

*/

// Solution:

class Character{
	constructor(username,role,guildName,level){
		this.username = username,
		this.role = role,
		this.guildName = guildName,
		this.level = level
	}
}

let character1 = new Character("Cayde-6","Gunslinger","Hunter",1305);
let character2 = new Character("Shaxx","Sentinel","Titan",1305);

console.log(character1);
console.log(character2);
