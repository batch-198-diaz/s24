// index.js

//let const
//var varSample = "Hoist me up!"
//Math.pow(base,exponent)



let string1 = "Javascript" 
let string2 = "not"
let string3 = "is"
let string4 = "Typescript"
let string5 = "Java"
let string6 = "Zuitt"
let string7 = "Coding"


let sentence1 = console.log(string1 + " " + string3 + " " + string2 + " " + string5);
let sentence2 = console.log(string4 + " " + string3 + " " + string1);


// Template Literals " " , ' '

// versus

// Template Literal ` ` and a placeholder ${}

let sentence3 = `${string1} ${string3} ${string2} ${string5}`;
let sentence4 = `${string4} ${string3} ${string1}`;
let sentence5 = `${string6} ${string7} Bootcamp`;

console.log(sentence3);
console.log(sentence4);
console.log(sentence5);

let sentence6 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence6);

let person = {
	name: "Michael",
	position: "developer",
	income: 50000,
	expenses: 60000
}

console.log(`${person.name} is a ${person.position}`);
console.log(`His income is ${person.income} and expenses at ${person.expenses}. His current balance is ${person.income - person.expenses}`);


// Array destructuring - order matters

let array1 = ["Curry","Lillard","Paul","Irving"];

// normal way
let player1 = array1[0];
let player2 = array1[1];
let player3 = array1[2];
let player4 = array1[3];
console.log(player1,player2,player3,player4);
//
//versus
//
// shorthand way
let [player5,player6,player7,player8] = array1;
console.log(player5,player6,player7,player8);
//

let array2 = ["Jokic","Embiid","Howard","Anthony-Towns"];

let [center1,center2,AnthonyTowns] = array2;
console.log(AnthonyTowns);

let [center3,center4,,center5] = array2;
console.log(center5);

let pokemon1 = {

	name: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf","Tackle","Leech Seed"]

}

let{level,type,name,moves,personality} = pokemon1;

console.log(`level: ${level}`);
console.log(`type: ${type}`);
console.log(`name: ${name}`);
console.log(`moves: ${moves}`);
console.log(`personality: ${personality}`);


let pokemon2 = {

	name: "Charmander",
	type: "Fire",
	level: 11,
	moves: ["Ember","Scratch"]
}

// Arrow functions - alternative way of writing functions with pros and cons

// normal way
function displayMsg(){
	console.log('Hello, World!');
}

displayMsg();
//
// versus
//
// arrow 
const hello = () => {
	console.log('Hello from Arrow!');
}

hello();

const greet = (friend) => {
	console.log(friend); //shows the whole object
	console.log(`Hi! ${friend.name}`); //right one
}

greet(person);

// Arrow vs Traditional Function

// Implicit Return - allows us to return a value from an arrow function without the use of return keyword and only works on arrow functions without {}

// traditional addNum() function

function addNum(num1,num2){

	//console.log(num1,num2);
	// let result = num1+num2;
	// return result

	return num1+num2
}

let sum = addNum(5,10);
console.log(sum);


// arrow function

let subNum = (num1,num2) => num1 - num2; //no curly braces
//putting curly brace means it's a code block so can't be used with one liners, it will output an undefined result so implicit return only works without {}

let diff = subNum(10,5);
console.log(diff);

/*

	Mini-Activity
	Translate our previous addNum traditional function into an arrow function with an implicit return.

*/

let addNum2 = (num1,num2) => num1 + num2;
let add = addNum(50,70);
console.log(add)



// Traditional functions vs Arrow Functions as Methods

let character1 = {
	name: "Cloud Strife",
	occupation: "SOLDIER",

	greet: function(){
		console.log(this); 
		// this keyword will refer current obj where the method is
		console.log(`Hi! I'm ${this.name}`);
	},
	introduceJob: () => {
		console.log(this); 
		// this keyword will NOT refer current obj but the global window
	}
}

character1.greet();
character1.introduceJob();

// Class Based Objects Blueprints
/*	- In Javascript, classes are templates of objects
	- We can create objects out of the use of classes
	- Before the introduction of classes in JS, we mimic this behaviour of being able to create objects of the same blueprint using constructor function.
*/

// Constructor Function

function Pokemon(name,type,level){
	this.name = name,
	this.type = type,
	this.level = level
}

// With the advent of ES6, we are now introduced to a special method of creating and initializing an object.

class Car {
	constructor(brand,name,year){

		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

let car1 = new Car("Toyota","Vios","2002");
let car2 = new Car("Copper","Mini","1969");
let car3 = new Car("Porsche","911","1967");

console.log(car1);
console.log(car2);
console.log(car3);

/*
	Mini-Activity:
	Translate the Pokemon constructor function as a Class Constructor then create 2 new pokemons out of the class constructor and save it in their respective variables. Log the values in the console.
*/

class Pokemon2{
	constructor(name,type,level){
		this.name = name,
		this.type = type,
		this.level = level
	}
}

let pokemon_1 = new Pokemon("Pichu","Electric","5");
let pokemon_2 = new Pokemon("Eevee","Normal","10");

console.log(pokemon_1);
console.log(pokemon_2);
